#include <stdint.h>
#include <time.h>
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "holiday_struct.h"

/* test_holidays.exe [Year [Region]]
 * */
int main(int argc, char*argv[]) {
	char* lib_name = "./holidays_DE.so";
	void *holidayLibrary;
	char *error;
	int year = 2018;
	
	void (*calculate_for_christian_year)(int, struct holiday *);
	
	// Load symbols from library
	holidayLibrary = dlopen(lib_name, RTLD_LAZY);
	if(!holidayLibrary) {
		printf("Konnte '%s' nicht laden.\n", lib_name);
		exit(EXIT_FAILURE);
	}
    char **regions = (char **) dlsym(holidayLibrary, "regionNames");
	if ((error = dlerror()) != NULL)  {
        printf("dlsym: %s\n", error);
        exit(EXIT_FAILURE);
    }
    struct holiday *holidays = (struct holiday*) dlsym(holidayLibrary, "holidays");
	if ((error = dlerror()) != NULL)  {
        printf("dlsym: %s\n", error);
        exit(EXIT_FAILURE);
    }
    *(void **) (&calculate_for_christian_year) = dlsym(holidayLibrary, "calculate_for_christian_year");
    
    
    // check whether parameter is given
    if(argc>1) {
		year = atoi(argv[1]);
	}
    
    char **p = regions;
    long int flag = 1;
    if(argc > 2)
		while(*p != 0)
		{
			if(strncmp(argv[2], *p, 128) == 0)
				break;
			p++;
			flag <<= 1;
		}
    
    // calculate Calendar
	(*calculate_for_christian_year)(year, holidays);
	
	fflush(stdout);
	for (size_t i = 0; holidays[i].name != 0; ++i) {
		if(argc <=2 || (holidays[i].regions1 & flag) == flag)
		{
			printf("%s\t%d.%d.\n", holidays[i].name, holidays[i].day, holidays[i].month);
			fflush(stdout);
		}
	}
	return 0;
}
