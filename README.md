# holiday-computation

Computes the holidays for a state.

Compile with:

make

start with:

test_holidays.exe 2021 Rheinland-Pfalz

OR

test_holidays.exe 2021

If you leave the year blank, it will default to 2018.