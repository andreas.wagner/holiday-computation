#include <stdint.h>
#include <time.h>

#include <stdio.h>

#include "holiday_struct.h"

enum Region {
	None = 0x0000000000000000,
	BW = 0x0000000000000001,	/* Baden-Württemberg */
	BY = 0x0000000000000002,	/* Bayern */
	BE = 0x0000000000000004,	/* Berlin */
	BB = 0x0000000000000008,	/* Brandenburg */
	HB = 0x0000000000000010,	/* Bremen */
	HH = 0x0000000000000020,	/* Hamburg */
	HE = 0x0000000000000040,	/* Hessen */
	MV = 0x0000000000000080,	/* Mecklenburg-Vorpommern */
	NI = 0x0000000000000100,	/* Niedersachsen */
	NW = 0x0000000000000200,	/* Nordrhein-Westfalen */
	RP = 0x0000000000000400,	/* Rheinland-Pfalz */
	SL = 0x0000000000000800,	/* Saarland */
	SN = 0x0000000000001000,	/* Sachsen */
	ST = 0x0000000000002000,	/* Sachsen-Anhalt */
	SH = 0x0000000000004000,	/* Schleswig-Holstein */
	TH = 0x0000000000008000,	/* Thüringen */
	DE = 0x000000000000ffff,	/* Deutschland */
};


const char* regionNames[] = {
	u8"Baden-Württemberg",
	u8"Bayern",
	u8"Berlin",
	u8"Brandenburg",
	u8"Bremen",
	u8"Hamburg",
	u8"Hessen",
	u8"Mecklenburg-Vorpommern",
	u8"Niedersachsen",
	u8"Nordrhein-Westfalen",
	u8"Rheinland-Pfalz",
	u8"Saarland",
	u8"Sachsen",
	u8"Sachsen-Anhalt",
	u8"Schleswig-Holstein",
	u8"Thüringen",
	0
};

struct holiday holidays[] = {
	//name, 				month,	day,	offset,	region1,	region2
	{u8"Neujahr",				1,		1,		400,	DE,			0}, // 0
	{u8"Heilige drei Könige",	1,		6,		400,	BW|BY|ST,	0}, // 1
	{u8"Gründonnerstag",		0,		0,		-3,		0,			0}, // 2
	{u8"Karfreitag",			0,		0,		-2,		DE,			0}, // 3
	{u8"Ostersonntag",			0,		0,		0,		BB,			0}, // 4
	{u8"Ostermontag",			0,		0,		1,		DE,			0}, // 5
	{u8"Tag der Arbeit",		5,		1,		400,	DE,			0}, // 6
	{u8"Christi Himmelfahrt",	0,		0,		39,		DE,			0}, // 7
	{u8"Pfingstsonntag",		0,		0,		49,		BB,			0}, // 8
	{u8"Pfingstmontag",			0,		0,		50,		DE,			0}, // 9
	{u8"Fronleichnam",			0,		0,		60,		BB|BY|HE|NW|RP|SL, 0}, // SN|TH ?
	{u8"Augsburger hohes Friedensfest",8,8,		400,	BY,			0}, // 11
	{u8"Mariä Himmelfahrt",		8,		15,		400,	BY|SL,		0}, // 12
	{u8"Tag der Deutschen Einheit",10,	3,		400,	DE,			0}, // 13
	{u8"Reformationstag",		10,		31,		400,	BB|HB|HH|MV|NI|SN|ST|SH|TH, 0},
	{u8"Allerheiligen",			11,		1,		400,	BW|BY|NW|RP|SL, 0},
	{u8"Buß- und Bettag",		0,		0,		400,	SN,			0}, // Wird anhand des Indexes (16) berechnet!! -- lt. Wikipedia Mittwoch vor dem 23. November
	{u8"Erster Weihnachtsfeiertag",12,	25,		400,	DE,			0}, // 17
	{u8"Zweiter Weihnachtsfeiertag",12,	26,		400,	DE,			0},
	{0, 0, 0, 0, 0, 0}
};

/*
char **getRegionNames() {
	return (char **) regionNames;
}

struct holiday* getHolidays() {
	return holidays;
}
*/

// Wednesday before Nov. 23.
void calculate_Buss_und_Bettag(struct holiday *holidays, int year, int offset) {
	struct tm struct_nov23 = {
		.tm_sec=0,
		.tm_min=0,
		.tm_hour=0,
		.tm_mday=23, 
		.tm_mon=10, // begins with 0
		.tm_year=year-1900,
		.tm_wday=-1, // ?
		.tm_yday=-1, // ?
		.tm_isdst=-1 // ?
	};
	/* 0	Sunday
	 * 1	Monday
	 * 2	Tuesday
	 * 3	Wednesday
	 * 4	Thursday
	 * 5	Friday
	 * 6	Saturday
	 * */
	struct tm *struct_wednesday_helper;
	time_t wednesday;
	time_t day = 60*60*24;
	
	time_t nov23 = mktime(&struct_nov23);
	struct_wednesday_helper = localtime(&nov23);
	wednesday = nov23 - (struct_wednesday_helper->tm_wday - 3)*day; // Should be a wednesday now.
	struct_wednesday_helper = localtime(&wednesday);
	
	holidays[offset].day = struct_wednesday_helper->tm_mday;
	holidays[offset].month = struct_wednesday_helper->tm_mon+1;
}

// Fullmoon1: 2018-03-02 01:51:02
struct tm timeinfo1 = {
	.tm_sec=2,
	.tm_min=51,
	.tm_hour=1,
	.tm_mday=2, 
	.tm_mon=2, // begins with 0
	.tm_year=118,
	.tm_wday=-1, // ?
	.tm_yday=-1, // ?
	.tm_isdst=-1 // ?
};

// Fullmoon2: 2018-03-31 14:36:54
struct tm timeinfo2 = {
	.tm_sec=54,
	.tm_min=36,
	.tm_hour=14,
	.tm_mday=31,
	.tm_mon=2, // begins with 0
	.tm_year=118,
	.tm_wday=-1, // ?
	.tm_yday=-1, // ?
	.tm_isdst=-1 // ?
};

// begin of spring
struct tm timeinfo3 = {
	.tm_sec=0,
	.tm_min=0,
	.tm_hour=0,
	.tm_mday=21,
	.tm_mon=2, // begins with 0
	.tm_year=-1, // set later
	.tm_wday=-1, // ?
	.tm_yday=-1, // ?
	.tm_isdst=-1, // ?
};

/** Compute eastersunday and apply correct dates to holidays where
 * offset is less than 365; 400 is a good value. */
// Compute first fullmoon after begin of spring (yyyy-03-21)
void calculate_for_christian_year(int year, struct holiday *holidays) {
	// Eastersunday
	int8_t easter_day = -1;
	int8_t easter_month = -1;
	time_t easter_helper;
	
	
	time_t week = 60*60*24*7;
	time_t day = 60*60*24;
	
	struct tm *helper_for_eastersunday;

	time_t fullmoon1 = mktime(&timeinfo1);
	time_t fullmoon2 = mktime(&timeinfo2);
	
	timeinfo3.tm_year=year-1900;

	time_t spring = mktime(&timeinfo3);
	
	time_t fullmoonPeriod = fullmoon2-fullmoon1;
	time_t counter = fullmoon1;
	if(spring < counter) {
		while(counter > spring) {
			counter -= fullmoonPeriod;
		}
		counter += fullmoonPeriod;
	}
	else {
		while(counter < spring) {
			counter += fullmoonPeriod;
		}
	}
	
	helper_for_eastersunday = localtime(&counter); // FIXME: local, UTC or italian?
	easter_helper = helper_for_eastersunday->tm_wday * day;
	counter -= easter_helper; // Should be a Sunday, now.
	counter += week; // FIXME: For every helper_for_eastersunday->tm_wday ?
	helper_for_eastersunday = localtime(&counter);
	easter_day = helper_for_eastersunday->tm_mday;
	easter_month = helper_for_eastersunday->tm_mon+1;
	
	// overwrite all dates where offset is less than 365.
	//int num = sizeof(holidays)/sizeof(struct holiday);
	//for( int i = 0; i < num; i++ ) {
	size_t i = 0;
	while(holidays[i].name != 0) {
		if(holidays[i].offset < 365) {
			struct tm timeinfo4;
			struct tm *timeinfo5;
			timeinfo4.tm_sec=0;
			timeinfo4.tm_min=0;
			timeinfo4.tm_hour=0;
			timeinfo4.tm_mday=easter_day;
			timeinfo4.tm_mon=easter_month-1;
			timeinfo4.tm_year=year-1900;
			timeinfo4.tm_wday=-1; // ?
			timeinfo4.tm_yday=-1; // ?
			timeinfo4.tm_isdst=-1; // ?
			
			time_t easter_time_t = mktime(&timeinfo4);
			time_t special_day_time_t = easter_time_t
								+ holidays[i].offset * day;
								
			timeinfo5 = localtime(&special_day_time_t); // FIXME: local, UTC or italian?
			holidays[i].day = timeinfo5->tm_mday;
			holidays[i].month = timeinfo5->tm_mon +1;
		}
		i++;
	}
	calculate_Buss_und_Bettag(holidays, year, 16);
}


