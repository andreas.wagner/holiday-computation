#ifndef __holidays_h__
#define __holidays_h__

struct holiday {
	char *name;
	uint8_t month;
	uint8_t day;
	int16_t offset;
	uint64_t regions1;
	uint64_t regions2;
};

#endif
