all: holidays_DE.so test_holidays.exe

holidays_DE.so: holidays_DE.c
	gcc -O3 -o $@ -shared -fPIC $^
	
test_holidays.exe: test_holidays.c
	gcc -O3 -o $@         $^ -ldl
